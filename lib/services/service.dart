import 'dart:convert';
import 'package:http/http.dart' as http;
import '../navigators/navigation_service.dart';

class IdService {
  //Login
  idLoginService(String phoneNumber, pass) async {
    Map data = {'login': phoneNumber, 'password': pass};
    var jsonResponse = null;
    var response = await http.post(
      Uri.parse("http://54.173.201.162:7678/api/auth/login"),
      body: data,
    );
    var idStatusCode = response.statusCode;
    if (idStatusCode == 201) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        NavigationService.pushNamedAndRemoveUntil('/test');
        print(response.body);
        print("status code: $idStatusCode");
      }
    } else {
      print(response.body);
      print("status code: $idStatusCode");
    }
  }

  //Register
  idRegisterService(String firstName, secondName, login, password, role) async {
    Map data = {
      'firstName': firstName,
      'secondName': secondName,
      'login': login,
      'password': password,
      'role': role
    };
    print(data);

    String body = json.encode(data);
    var response = await http.post(
      Uri.parse("http://54.173.201.162:7678/api/auth/create"),
      body: body,
    );
    var idStatusCode = response.statusCode;
    print(response.body);
    print(idStatusCode);
    if (idStatusCode >= 200 && idStatusCode < 300) {
      print('Success');
    } else {
      print('Eroor code: $idStatusCode');
      print('Body: ${response.body}');
    }
  }

  //Send OTP
  idOTPService(String login) async {
    Map data = {'login': login};
    print(data);

    var response = await http.put(
      Uri.parse("http://54.173.201.162:7678/api/auth/send/otp"),
      body: data,
    );
    var idStatusCode = response.statusCode;
    var jsonResponse = null;
    print(response.body);
    print(idStatusCode);
    if (idStatusCode == 200) {
      jsonResponse = json.decode(response.body);
      if (jsonResponse != null) {
        // NavigationService.pushNamedAndRemoveUntil('/test');
        print(response.body);
        print("status code: $idStatusCode");
      }
    } else {
      print(response.body);
      print("status code: $idStatusCode");
    }
  }
}
