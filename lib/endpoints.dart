class Endpoints {
  static const String appName = "Casting";
  static const String apiURL = "http://54.173.201.162:7678/api";
  static const String loginAPI = "/auth/login";
  static const String registerAPI = "/auth/create";
  static const String userProfileAPI = "/auth/profile";
}