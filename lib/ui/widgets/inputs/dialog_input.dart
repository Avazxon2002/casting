import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../theme/style.dart';

class DialogInputWidget extends StatelessWidget {
  const DialogInputWidget({Key? key,
    required this.idTextInputType,
    required this.idObscureText, required this.controller, required this.hintText,})
      : super(key: key);

  final TextInputType idTextInputType;
  final bool idObscureText;
  final TextEditingController controller;
  final String hintText;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.h,
      child: TextFormField(
        controller: controller,
        style: TextStyle(fontSize: 12.sp),
        keyboardType: idTextInputType,
        obscureText: idObscureText,
        decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1.w, color: idPrimaryGray),
              borderRadius: BorderRadius.circular(12.r),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1.w, color: idPrimaryGray),
              borderRadius: BorderRadius.circular(12.r),
            ),
            hintText: hintText
        ),
      ),
    );
  }
}
