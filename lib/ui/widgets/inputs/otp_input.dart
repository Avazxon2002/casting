import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../theme/style.dart';

class OTPDecoInput extends StatelessWidget {
  const OTPDecoInput({Key? key,
    required this.hintText,
    required this.idTextInputType,
    required this.idObscureText,
    this.idController,
    this.validator, this.iconButton,
    this.onSaved})
      : super(key: key);

  final String hintText;
  final TextInputType idTextInputType;
  final bool idObscureText;
  final FormFieldValidator<String>? validator;
  final FormFieldSetter<String>? onSaved;
  final IconButton? iconButton;
  final TextEditingController? idController;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50.h,
      child: TextFormField(
        controller: idController,
        style: TextStyle(fontSize: 12.sp),
        keyboardType: idTextInputType,
        obscureText: idObscureText,
        validator: validator,
        onSaved: onSaved,
        decoration: InputDecoration(
          // prefixIcon: Column(
          //   mainAxisAlignment: MainAxisAlignment.center,
          //   children: const [
          //     Text("+998"),
          //   ],
          // ),
          suffixIcon: iconButton,
          border: OutlineInputBorder(
            borderSide: BorderSide(width: 1.w, color: idPrimaryGray),
            borderRadius: BorderRadius.circular(12.r),
          ),
          hintText: hintText,
        ),
      ),
    );
  }
}
