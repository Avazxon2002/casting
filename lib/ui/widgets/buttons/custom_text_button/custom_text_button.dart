import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../theme/style.dart';

class IdCustomTextButton extends StatelessWidget {
  const IdCustomTextButton(
      {Key? key, required this.idText, required this.onPress})
      : super(key: key);
  final String idText;
  final Function onPress;

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: TextButton(
        onPressed: () {
          onPress;
        },
        child: Text(
          idText,
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 12.sp,
            fontWeight: FontWeight.w500,
            color: idPrimaryBlue,
          ),
        ),
      ),
    );
  }
}
