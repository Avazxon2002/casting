import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButton extends StatelessWidget {
  const CustomButton(
      {Key? key,
      required this.idButtonName,
      required this.idButtonColor,
      required this.idButtonTxtColor,
      required this.idButtonPress})
      : super(key: key);

  final String idButtonName;
  final Color idButtonColor;
  final Color idButtonTxtColor;
  final VoidCallback idButtonPress;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        fixedSize: Size(390.w, 55.h),
        primary: idButtonColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.r),
        ),
      ),
      onPressed: idButtonPress,
      child: Text(
        idButtonName,
        style: TextStyle(
          color: idButtonTxtColor,
          fontFamily: 'Poppins',
          fontSize: 20.sp,
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
