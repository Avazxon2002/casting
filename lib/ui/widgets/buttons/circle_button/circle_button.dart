import 'package:casting/ui/widgets/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import '../../../screens/entry/login_screen/login_page.dart';

class CircleButton extends StatelessWidget {
  const CircleButton({
    Key? key,
    required this.currentPage,
    required this.pageController,
  }) : super(key: key);
  final int currentPage;
  final PageController pageController;

  @override
  Widget build(BuildContext context) {
    return CircularPercentIndicator(
      radius: 50.r,
      lineWidth: 3.5.w,
      circularStrokeCap: CircularStrokeCap.round,
      percent: double.parse('$currentPage') / 2,
      animateFromLastPercent: true,
      animation: true,
      progressColor: idPrimaryGreen,
      backgroundColor: idPrimaryGreen.withOpacity(0.5),
      center: ElevatedButton(
        onPressed: () {
          if (currentPage == 2) {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => LoginPage(),
              ),
            );
          } else {
            pageController.nextPage(
                duration: const Duration(milliseconds: 300),
                curve: Curves.easeInOutQuint);
          }
        },
        style: ElevatedButton.styleFrom(
          shape: const CircleBorder(),
          padding: EdgeInsets.all(20.h),
          primary: idPrimaryGreen,
        ),
        child: Icon(Icons.arrow_forward, color: Colors.white, size: 30.sm,),
      ),
    );
  }
}
