import 'package:casting/ui/widgets/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomDropDown extends StatefulWidget {
  const CustomDropDown({Key? key}) : super(key: key);

  @override
  State<CustomDropDown> createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  List<DropdownMenuItem<String>> get dropdownItems {
    List<DropdownMenuItem<String>> menuItems = [
      const DropdownMenuItem(
        value: "Actor",
        child: Text(
          "Actor",
          style: TextStyle(fontFamily: 'Poppins'),
        ),
      ),
      const DropdownMenuItem(
        value: "Filmmaker",
        child: Text(
          "Filmmaker",
          style: TextStyle(fontFamily: 'Poppins'),
        ),
      ),
    ];
    return menuItems;
  }

  String? selectedValue;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 70.h,
        child: DropdownButtonFormField(
          decoration: InputDecoration(
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: idPrimaryGray, width: 1.w),
              borderRadius: BorderRadius.circular(12.r),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(width: 1.w, color: idPrimaryGray),
              borderRadius: BorderRadius.circular(12.r),
            ),
          ),
          hint: Text("Choose", style: TextStyle(fontSize: 12.sp)),
          style: TextStyle(fontSize: 12.sp, color: Colors.black),
          value: selectedValue,
          onChanged: (String? value) {
            setState(() {
              selectedValue = value;
            });
          },
          items: dropdownItems,
        )

        // FormHelper.dropDownWidget(
        //   context,
        //   "Select position",
        //   positionId,
        //   postions,
        //   (onChangedValue) {
        //     positionId = onChangedValue;
        //     print("Selected position: $onChangedValue");
        //   },
        //   (onValidateValue) {
        //     if (onValidateValue == null) {
        //       return 'Please Select Position';
        //     }
        //     return null;
        //   },
        //   borderColor: idPrimaryGray,
        //   borderFocusColor: idPrimaryGray,
        //   paddingLeft: 0,
        //   paddingRight: 0,
        //   borderRadius: 12.r,
        //   optionValue: "id",
        //   optionLabel: "label",
        //   hintFontSize: 25,
        //
        // ),
        );
  }
}
