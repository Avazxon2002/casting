import 'package:casting/ui/screens/main/pages/home_page.dart';
import 'package:flutter/material.dart';
import '../../screens/main/pages/home_page.dart';

class BottomTab extends StatefulWidget {
  const BottomTab({Key? key}) : super(key: key);

  @override
  State<BottomTab> createState() => _BottomTabState();
}

class _BottomTabState extends State<BottomTab> {
  int _currentIndex = 0;
  List body = [
    const HomePage(),
    const Icon(Icons.receipt_outlined),
    const Icon(Icons.apps_sharp),
    const Icon(Icons.message),
    const Icon(Icons.person),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: body[_currentIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentIndex,
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Colors.red,
        unselectedItemColor: Colors.black,
        items: const [
          BottomNavigationBarItem(
            label: 'Home',
            icon: Icon(
              Icons.home,
            ),
          ),
          BottomNavigationBarItem(
            label: 'Resume',
            icon: Icon(
              Icons.receipt_outlined,
            ),
          ),
          BottomNavigationBarItem(
            label: 'Projects',
            icon: Icon(
              Icons.apps_sharp,
            ),
          ),
          BottomNavigationBarItem(
            label: 'News',
            icon: Icon(
              Icons.message,
            ),
          ),
          BottomNavigationBarItem(
            label: 'Profile',
            icon: Icon(
              Icons.person,
            ),
          ),
        ],
        onTap: (int newIndex) {
          setState(() {
            _currentIndex = newIndex;
          });
        },
      ),
    );
  }
}
