import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomDialog extends StatelessWidget {
  const CustomDialog({Key? key, required this.idTitle, required this.idContent}) : super(key: key);

  final String idTitle;
  final Widget idContent;
  // final Widget idAction1;
  // final Widget idAction2;

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.r))),
      title: Text(idTitle),
      content: idContent,
    );
  }
}
