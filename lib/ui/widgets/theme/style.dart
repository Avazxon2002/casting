import 'package:flutter/material.dart';

const Color idPrimaryBlue = Color(0xff757DE8);
const Color idBlue = Color(0xff3F51B5);
const Color idBackgroundGray = Color(0xfff1f1f1);
const Color idPrimaryGray = Color(0xff7D7D7D);
const Color idPrimaryGreen = Color(0xffA4A4A4);
const Color idPrimaryBlueOpacity = Color(0xffdce7fc);
const Color idPrimaryGrayOpacity = Color(0xffd9e0f6);
