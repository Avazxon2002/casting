import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../../../widgets/theme/style.dart';

class OnboardingComponent extends StatelessWidget {
  const OnboardingComponent({
    Key? key,
    required this.title,
    required this.description,
    required this.image,
  }) : super(key: key);

  final String? title, description, image;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        Image.asset(
          image!,
          height: 260.h,
        ),
        SizedBox(height: 10.h),
        Text(
          title!,
          style: TextStyle(
            fontFamily: 'Poppins',
            fontSize: 30.sp,
            fontWeight: FontWeight.bold,
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.w),
          child: Text(
            description!,
            style: TextStyle(
              fontFamily: 'Poppins',
              fontSize: 15.sp,
              fontWeight: FontWeight.w600,
              color: idPrimaryGray,
            ),
            textAlign: TextAlign.center,
          ),
        )
      ],
    );
  }
}
