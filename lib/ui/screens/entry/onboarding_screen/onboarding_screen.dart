import 'package:casting/ui/screens/entry/onboarding_screen/pages/onboarding_page.dart';
import 'package:flutter/material.dart';

class OnboardingScreen extends StatelessWidget {
  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: OnboardingPage(),
    );
  }
}
