import 'package:casting/ui/widgets/buttons/circle_button/circle_button.dart';
import 'package:casting/ui/widgets/theme/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../onboarding_component.dart';

class OnboardingPage extends StatefulWidget {
  const OnboardingPage({Key? key}) : super(key: key);

  @override
  State<OnboardingPage> createState() => _OnboardingPageState();
}

class _OnboardingPageState extends State<OnboardingPage> {
  int _currentPage = 0;
  final PageController _pageController = PageController();
  List<Map<String, String>> onboardingData = [
    {
      "title": "Resume",
      "description":
          "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.",
      "image": "assets/images/onboarding/resume.png",
    },
    {
      "title": "Photo-Video Samples",
      "description":
          "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.",
      "image": "assets/images/onboarding/photo_video.png",
    },
    {
      "title": "Creative",
      "description":
          "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor.",
      "image": "assets/images/onboarding/creative.png",
    },
  ];

  _onChanged(int index) {
    setState(() {
      _currentPage = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            // Images & Texts
            Expanded(
              flex: 4,
              child: PageView.builder(
                controller: _pageController,
                onPageChanged: _onChanged,
                itemCount: onboardingData.length,
                itemBuilder: (context, index) => OnboardingComponent(
                  title: onboardingData[index]["title"],
                  description: onboardingData[index]["description"],
                  image: onboardingData[index]["image"],
                ),
              ),
            ),

            //Dots & Button
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: Column(
                  children: [
                    SizedBox(height: 40.h),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        onboardingData.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    SizedBox(height: 30.h),
                    CircleButton(
                      currentPage: _currentPage,
                      pageController: _pageController,
                    ),
                    const Spacer(flex: 3),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: const Duration(milliseconds: 300),
      margin: EdgeInsets.only(right: 5.w),
      height: 10.h,
      width: (_currentPage == index) ? 20.w : 10.w,
      decoration: BoxDecoration(
        color: (_currentPage == index) ? idPrimaryBlue : idPrimaryGreen,
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}
