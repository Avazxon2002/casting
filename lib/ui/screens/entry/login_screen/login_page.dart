import 'package:casting/navigators/navigation_service.dart';
import 'package:casting/services/service.dart';
import 'package:casting/utils/validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../widgets/buttons/custom_button/custom_button.dart';
import '../../../widgets/buttons/custom_text_button/custom_text_button.dart';
import '../../../widgets/inputs/custom_input.dart';
import '../../../widgets/theme/style.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final IdService _idService = IdService();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  bool isApiCallProcess = false;
  bool hidePassword = true;
  String? login;
  String? password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Welcome,",
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 40.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Text(
                        "Login to continue!",
                        style: TextStyle(
                          fontFamily: 'Poppins',
                          fontSize: 20.sp,
                          fontWeight: FontWeight.w600,
                          color: idPrimaryGray,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20.h),
                    child: buildLoginInput(),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10.h),
                    child: buildLoginButtons(),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  ///Inputs
  Widget buildLoginInput() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 15.w),
          child: Text(
            "Phone Number",
            style: TextStyle(
              color: idPrimaryGray,
              fontSize: 15.sp,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        CustomInput(
          idController: phoneNumberController,
          hintText: "Enter your phone number",
          idTextInputType: TextInputType.phone,
          idObscureText: false,
          validator: (onValidateVal) {
            if (onValidateVal!.isEmpty) {
              return 'Phone number can\'t be empty.';
            }
            return null;
          },
          onSaved: (onSavedVal) => {
            login = onSavedVal,
          },
        ),
        SizedBox(
          height: 10.h,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.w),
          child: Text(
            "Password",
            style: TextStyle(
              color: idPrimaryGray,
              fontSize: 15.sp,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        CustomInput(
          idController: passwordController,
          hintText: "Enter your password",
          idTextInputType: TextInputType.visiblePassword,
          idObscureText: hidePassword,
          iconButton: IconButton(
            onPressed: () {
              setState(() {
                hidePassword = !hidePassword;
              });
            },
            color: Colors.white.withOpacity(0.7),
            icon: Icon(
              hidePassword ? Icons.visibility_off : Icons.visibility,
              color: Colors.black,
              size: 15.sp,
            ),
          ),
          validator: (value) {
            return IdValidator.validationPassword(value ?? "");
          },
          onSaved: (onSavedVal) => {
            password = onSavedVal,
          },
        ),
        IdCustomTextButton(
          idText: "Forgot password ?",
          onPress: () {},
        ),
      ],
    );
  }

  ///Buttons
  Widget buildLoginButtons() {
    return Column(
      children: [
        CustomButton(
          idButtonName: "Login",
          idButtonColor: idPrimaryBlue,
          idButtonTxtColor: Colors.white,
          idButtonPress: () {
            setState(() {
              _idService.idLoginService(phoneNumberController.text, passwordController.text);
            });
          },
        ),
        SizedBox(
          height: 20.h,
        ),
        CustomButton(
          idButtonName: "Sign Up",
          idButtonColor: idPrimaryBlueOpacity,
          idButtonTxtColor: idBlue,
          idButtonPress: () {
            NavigationService.navigateTo('/otp');
          },
        ),
      ],
    );
  }

  ///Validation
  bool validateAndSave() {
    final form = _formKey.currentState;
    if (form!.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

}
