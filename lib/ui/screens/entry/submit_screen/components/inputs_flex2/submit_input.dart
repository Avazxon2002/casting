import 'package:casting/ui/widgets/inputs/custom_input.dart';
import 'package:flutter/material.dart';
import '../../../../../widgets/theme/style.dart';

class SubmitInput extends StatelessWidget {
  const SubmitInput({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text(
          "+998 91 200 28 23",
          style: TextStyle(
            fontFamily: 'Poppins',
            fontWeight: FontWeight.w600,
            fontSize: 22,
          ),
        ),
        Padding(
          padding: EdgeInsets.only(left: 15),
          child: Text(
            "Password",
            style: TextStyle(
              color: idPrimaryGray,
              fontSize: 15,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        CustomInput(
          hintText: "Enter your password",
          idTextInputType: TextInputType.visiblePassword,
          idObscureText: true,
        ),
        SizedBox(
          height: 10,
        ),
        Padding(
          padding: EdgeInsets.only(left: 15),
          child: Text(
            "Repeat Password",
            style: TextStyle(
              color: idPrimaryGray,
              fontSize: 15,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        CustomInput(
          hintText: "Repeat your password",
          idTextInputType: TextInputType.visiblePassword,
          idObscureText: true,
        ),
      ],
    );
  }
}
