import 'package:casting/ui/widgets/theme/style.dart';
import 'package:flutter/material.dart';
import '../../../../../widgets/buttons/custom_button/custom_button.dart';

class SubmitButton extends StatelessWidget {
  const SubmitButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomButton(
          idButtonName: "Submit",
          idButtonColor: idPrimaryBlue,
          idButtonTxtColor: Colors.white,
          idButtonPress: () => {
            print("Login Clicked"),
          },
        ),
      ],
    );
  }
}
