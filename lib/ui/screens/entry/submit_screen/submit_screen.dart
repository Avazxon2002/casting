import 'package:casting/ui/screens/entry/submit_screen/pages/submit_page.dart';
import 'package:flutter/material.dart';

class SubmitScreen extends StatelessWidget {
  const SubmitScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: SubmitPage(),
    );
  }
}
