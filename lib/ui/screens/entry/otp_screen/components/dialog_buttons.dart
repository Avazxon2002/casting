import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class DialogButtons extends StatelessWidget {
  const DialogButtons(
      {Key? key,
        required this.idButtonName,
        required this.idButtonColor,
        required this.idButtonTxtColor,
        required this.idButtonPress})
      : super(key: key);

  final String idButtonName;
  final Color idButtonColor;
  final Color idButtonTxtColor;
  final VoidCallback idButtonPress;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        fixedSize: Size(65.w, 20.h),
        primary: idButtonColor,
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.r),
        ),
      ),
      onPressed: idButtonPress,
      child: Text(
        idButtonName,
        style: TextStyle(
          color: idButtonTxtColor,
          fontSize: 12.sp,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w500,
        ),
      ),
    );
  }
}
