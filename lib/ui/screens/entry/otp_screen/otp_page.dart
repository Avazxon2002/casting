import 'package:casting/navigators/navigation_service.dart';
import 'package:casting/ui/screens/entry/otp_screen/components/dialog_buttons.dart';
import 'package:casting/ui/widgets/buttons/custom_text_button/custom_text_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import '../../../../services/service.dart';
import '../../../widgets/buttons/custom_button/custom_button.dart';
import '../../../widgets/dialog/custom_dialog.dart';
import '../../../widgets/inputs/dialog_input.dart';
import '../../../widgets/inputs/otp_input.dart';
import '../../../widgets/theme/style.dart';

class OTPPage extends StatefulWidget {
  OTPPage({Key? key}) : super(key: key);

  @override
  State<OTPPage> createState() => _OTPPageState();
}

class _OTPPageState extends State<OTPPage> {

  final IdService _idService = IdService();
  final TextEditingController phoneNumberController = TextEditingController();
  final TextEditingController code = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: SizedBox(
          width: double.infinity,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Create Account,",
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 40.sp,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "Sign up to get started!",
                      style: TextStyle(
                        fontFamily: 'Poppins',
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w600,
                        color: idPrimaryGray,
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20.h),
                  child: buildSendOTPInput(),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 10.h),
                  child: buildSendOTPButton(context),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //Sent OTP Button
  Widget buildSendOTPButton(BuildContext context) {
    return Column(
      children: [
        CustomButton(
            idButtonName: "Sign Up",
            idButtonColor: idPrimaryBlueOpacity,
            idButtonTxtColor: idBlue,
            idButtonPress: () {
              setState(() {
                _idService.idOTPService(phoneNumberController.text);
              });
              showDialog(
                context: context,
                builder: (BuildContext context) => CustomDialog(
                  idTitle: "idTitle",
                  idContent: buildDialogInput(context),
                ),
              );
            }),
        SizedBox(height: 20.h),
        CustomButton(
          idButtonName: "Login",
          idButtonColor: idPrimaryBlue,
          idButtonTxtColor: Colors.white,
          idButtonPress: () => {print("Login Clicked")},
        ),
      ],
    );
  }

  //Send OTP Input
  Widget buildSendOTPInput() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(left: 15.w),
          child: Text(
            "Phone Number",
            style: TextStyle(
              color: idPrimaryGray,
              fontSize: 15.sp,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
        OTPDecoInput(
          idController: phoneNumberController,
          hintText: "Enter your phone number",
          idTextInputType: TextInputType.phone,
          idObscureText: false,
        ),
      ],
    );
  }

  //Dialog Input
  Widget buildDialogInput(BuildContext buildContext) {
    return SizedBox(
      height: 150.h,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(left: 15.w),
            child: Text(
              "Code",
              style: TextStyle(
                color: idPrimaryGray,
                fontSize: 15.sp,
                fontFamily: 'Poppins',
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          DialogInputWidget(
            idTextInputType: TextInputType.number,
            idObscureText: false,
            controller: code,
            hintText: 'Enter the code',
          ),

          //resend code
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              const Text("Resend code"),
              IdCustomTextButton(
                  idText: "in 29 sec",
                  onPress: () {
                    print("In 29 sec clicked");
                  })
            ],
          ),

          //buttons
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TextButton(
                onPressed: () => Navigator.pop(buildContext, "Cancel"),
                child: const Text("Cancel"),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 8.0),
                child: DialogButtons(
                  idButtonName: 'Verify',
                  idButtonColor: idPrimaryBlue,
                  idButtonTxtColor: Colors.white,
                  idButtonPress: () {
                    NavigationService.goBack();
                  },
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
