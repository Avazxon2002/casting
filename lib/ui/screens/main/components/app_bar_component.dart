import 'package:flutter/material.dart';
import '../../../widgets/theme/style.dart';

class AppBarComponent extends StatefulWidget implements PreferredSizeWidget {
  const AppBarComponent({Key? key, this.idLeading})
      : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  final Widget? idLeading;

  @override
  final Size preferredSize; // default is 56.0

  @override
  _AppBarState createState() => _AppBarState();
}

class _AppBarState extends State<AppBarComponent> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: idPrimaryGrayOpacity,
      leading: widget.idLeading,
      actions: const [
        Icon(
          Icons.notifications_none,
          color: Colors.black,
        ),
        SizedBox(
          width: 5,
        ),
        Padding(
          padding: EdgeInsets.all(8.0),
          child: CircleAvatar(
            backgroundImage:
                AssetImage("assets/images/custom/custom_avatar.png"),
            radius: 21,
          ),
        )
      ],
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: const [
              Text(
                "Andrew",
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "Houston",
                style: TextStyle(
                  fontFamily: 'Poppins',
                  fontWeight: FontWeight.w600,
                  fontSize: 20,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          const Text(
            "Actor",
            style: TextStyle(
              fontFamily: 'Poppins',
              fontWeight: FontWeight.w500,
              fontSize: 15,
              color: idPrimaryGray,
            ),
          ),
        ],
      ),
    );
  }
}
