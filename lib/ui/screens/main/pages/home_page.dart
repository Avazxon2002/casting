import 'package:flutter/material.dart';
import '../components/app_bar_component.dart';
import '../components/card_component.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(62),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            AppBarComponent(),
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 0.80,
          ),
          itemCount: 7,
          itemBuilder: (context, index) {
            return const CardComponent();
          },
        ),
      ),
    );
  }
}
