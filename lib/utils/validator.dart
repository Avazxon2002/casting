class IdValidator {

  //Phone Number
  static String? validationPhoneNumber(String value) {
    if(value.isEmpty) {
      return 'Please enter a valid phone number.';
    } else {
      return null;
    }
  }

  //Password
  static String? validationPassword(String value) {
    if(value.isEmpty) {
      return 'Please enter a valid password.';
    } else {
      return null;
    }
  }
}