import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'navigators/navigation_service.dart';
import 'navigators/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      builder: (BuildContext context, Widget? child) => MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          fontFamily: 'Poppins',
          primarySwatch: Colors.blue,
        ),
        navigatorKey: NavigationService.navigationKey,
        routes: Routes.routes,
        initialRoute: Routes.splash,
        // home: const SplashScreen(),
      ),
      designSize: const Size(360, 690),
    );
  }
}
